const pila = [];

function addQueue(name){
	const person = name;

	pila.push(person);
	return "Added in Queue: " + person;
}

function showAll(){
	return pila;
}

function deQueue(){
	pila.shift();
	return "Deleted First Person";
}

function expediteIndex(index){
	pila.splice(index, 1);
	return "Expedited."
}

function reverseDequeue(){
	pila.pop();
	return "Deleted last person";
}

function addVIP(VIPname){
	const newVIP = VIPname;
	pila.unshift(newVIP);
	return "VIP Added: " + VIPname;
}

function updateName(index, Newname){
	pila[index] = Newname;
	return "Successfully Added: " + Newname;
}