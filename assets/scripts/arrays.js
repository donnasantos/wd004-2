// // Array is a collection of related data denoted by []
// // Ex.

// const fruits = ['Apple', 'Banaba', 'Kiwi'];
// const ages = [1, 2, 3, 4];
// const students = [ {name: "brandon", age: 11}, {name: "Brenda", age: 12}];

// // It is recommended to use plural form when naming an array, and singular when namin an object

// // push-- add / end
// // pop -- delete / end
// // shift -- delete/ start
// // unshift - add / start

// // .splice();
// // It takes at least 2 arguments 
// // array.splice (starting index, no. of items you want to delete)\

const toDos = [];

function addToDo (task){

const newToDo = task;

toDos.push(newToDo);
return "successfully added " + newToDo;

}

function showToDos(){
	return toDos;
}

function updateToDo(index, updatedTask){
	todos[index]= updatedTask;
	return "successfully updated task!"

}

function deleteToDo(index, updatedTask){

	toDos.splice(index, 1);
	return "Successfully Deleted Task!";

}