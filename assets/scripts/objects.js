// Data Types
// 1. String
// 2. Number
// 3. Boolean
// 4. Undefined - functions without return value, variables that are declared but withou assigned values
// 5. null - no value. Explicit, if the operation cannot find a value
// 6. Objects - collection of related values. denoted by {}. Presented in a key-value pair

const student = {
	Name: "Donna",
	studentNumber: "2011-14202",
	course: "BS Engineering",
	department: "Electrical",
	age: 29,
	isSingle: false,
	motto: "Time is Gold",
	address: {
		stName: "Mahigany St.",
		city: "Makati City",
		Brgy: "Poblacion",
		zipCode: 1600
	},
	showAge: function(){
		console.log(student.age);
		return student.age;
	},
	addAge: function(){
		//add 1 to the age property of student
		student.age += 1;
		return "successfully added age";

	}
};

const student2 ={
	name: "Brenda"
}

// A function inside a function is called method


//Use dot notation to access a specific property of an object
//dot notation that does not exist will cause an error
//if we accessa property that does noy exist, it will return undefined
//to add proporty in an object we will use dot notation and assign the value

student.gender = "Female";

// To update

student.course = "BS Biology";

// to delete

delete student.studentNumber;