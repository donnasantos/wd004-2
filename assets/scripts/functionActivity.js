function getSum(num1, num2){
	const sum = num1 + num2;
	return sum;
}

function getDifference(num1, num2){
	const difference = num1 - num2;
	return difference;
}

function getProduct (num1, num2){
	const product = num1 * num2;
	return product;
}

function getQuotient (num1, num2){
	const quotient = num1 / num2;
	return quotient;
}



function getCylinderVolume (radius, height){
	const pi = 3.1415926;
	const sqradius = radius * radius;
	const piandrad = sqradius * pi;
	const cylindervolume = piandrad * height;
	return cylindervolume;
}

function getTrapezoidArea (base1, base2, height){
	const sumbase = base1 * base2;
	const baseovertwo = sumbase / 2;
	const trapezoidarea = baseovertwo * height;
	return trapezoidarea;
}	

function getTotalAmount (cost, qty, discount, tax){
	const amount = cost * qty;
	const percentDiscount = discount / 100;
	const percentTax = tax / 100;
	const discountAmount = amount * percentDiscount;
	const amountWithoutTax = amount - discountAmount;
	const taxAmount = amountWithoutTax * percentTax;
	const totalAmount = amountWithoutTax + taxAmount;
	return totalAmount;
}

function getTotalSalary(monthSalary, workingDays, daysAbsent, minLate, tax){
	const daySalary = monthSalary / workingDays;
	const hourlySalary = daySalary / 8;
	const minuteSalary = hourlySalary / 60;
	const lateDeduction = minLate * minuteSalary;
	const absentDeduction = daysAbsent * daySalary;
	const totalDeductions = absentDeduction + lateDeduction;
	const grossSalary = monthSalary - totalDeductions;
	const percentTax = tax / 100;
	const taxAmount = percentTax * grossSalary;
	const totalSalary = grossSalary - taxAmount;
	return totalSalary;
}